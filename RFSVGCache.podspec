#
# Be sure to run `pod lib lint RFSVGCache.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RFSVGCache'
  s.version          = '1.0.7'
  s.summary          = 'Cache lib for SVG resources.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'Cache system for SVG icons building using PocketSVG.'

  s.homepage         = 'https://gitlab.com/fillin/rfsvgcache'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jeka-mel' => 'iosdeveloper.mail@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/fillin/rfsvgcache.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.swift_version = '5.0'

  s.source_files = 'RFSVGCache/Classes/**/*'
  
  # s.resource_bundles = {
  #   'RFSVGCache' => ['RFSVGCache/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
  s.dependency 'PocketSVG'
end
